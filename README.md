# convertmusic

Convert a source archive music set into an encoded directory of music, taking
care of illegal characters and doing conversion in parallel if you wish.
